import { FC } from 'react';

import { useGetExpressionsQuery } from 'entities/expressions';
import { ValidateQuery } from 'features';

import { ExpressionsList } from '../expression-list/ui/expression-list';

export const Expressions: FC = () => {
  const values = useGetExpressionsQuery();

  return (
    <ValidateQuery w="w-[300px]" h="h-[456px]" {...values}>
      <ExpressionsList />
    </ValidateQuery>
  );
};
