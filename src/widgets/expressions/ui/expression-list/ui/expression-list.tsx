import { FC, useEffect } from 'react';

import { useGetExpressionsQuery, selectedExpressionSliceActions } from 'entities/expressions';
import { useAppDispatch } from 'shared/hooks';

import { ExpressionListItem } from './expression-list-item';

const { setSelectedId } = selectedExpressionSliceActions;

export const ExpressionsList: FC = () => {
  const { data, isSuccess } = useGetExpressionsQuery();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (isSuccess && data?.length) {
      dispatch(setSelectedId(data[data.length - 1]._id));
    }
  }, [isSuccess]);

  return (
    <div className="overflow-auto">
      {data?.map(({ date, expressions, _id }, i) => {
        return (
          <ExpressionListItem key={_id} date={date} expressions={expressions} id={_id} i={i} />
        );
      })}
    </div>
  );
};
