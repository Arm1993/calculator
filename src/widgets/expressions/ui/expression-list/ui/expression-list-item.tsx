import { FC } from 'react';

import { RExpressionResponse, selectedExpressionSliceSelectors } from 'entities/expressions';
import { useAppSelector } from 'shared/hooks';
import { SelectExpressionButton } from 'features/expressions';

import { Block } from '../../block/block';
import { Item } from '../../item/item';

interface IProps {
  id: string;
  i: number;
  expressions: RExpressionResponse['expressions'];
  date: RExpressionResponse['date'];
}

export const ExpressionListItem: FC<IProps> = ({ expressions, i, id, date }) => {
  const selectedId = useAppSelector(selectedExpressionSliceSelectors.selectSelectedId);
  const mt = i && 'mt-[20px]';
  const selectedClass = selectedId === id ? 'bg-purple-700' : 'bg-purple-500';
  return (
    <div>
      <SelectExpressionButton
        className={`${selectedClass}  text-white font-bold flex justify-center items-center text-center ${mt} h-[40px] ${selectedClass}`}
        id={id}
      >
        {date}
      </SelectExpressionButton>
      {id === selectedId && (
        <Block>
          {expressions.map(({ expression, _id }) => {
            return <Item key={_id}>{expression}</Item>;
          })}
        </Block>
      )}
    </div>
  );
};
