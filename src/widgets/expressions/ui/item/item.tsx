import { TFCChildren } from 'shared/model';

export const Item: TFCChildren = ({ children }) => {
  return <li className="mx-[10px] p-2.5 bg-slate-300 rounded-md break-words">{children}</li>;
};
