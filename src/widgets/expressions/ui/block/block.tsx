import { TFCChildren } from 'shared/model';

export const Block: TFCChildren = ({ children }) => {
  return <ul className="flex flex-col gap-2 bg-light-gray">{children}</ul>;
};
