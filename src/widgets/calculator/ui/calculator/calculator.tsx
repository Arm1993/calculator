import { FC } from 'react';

import {
  EOperations,
  FLOAT_CHARS,
  EqualAndResetPanelTemplate,
  NumericPanelTemplate,
  OperationsPanelTemplate,
  ResultPanelTemplate,
} from 'entities/panels';
import {
  EqualButton,
  ResetButton,
  NumericButton,
  ResultPanel,
  OperationButton,
} from 'features/panels';

export const Calculator: FC = () => {
  return (
    <div data-testid="Panels" data-cyid="panels" className="flex w-[240px] flex-col gap-[12px]">
      <ResultPanelTemplate>
        <ResultPanel />
      </ResultPanelTemplate>
      <OperationsPanelTemplate
        divisionButton={<OperationButton>{EOperations.DIVISION}</OperationButton>}
        minusButton={<OperationButton>{EOperations.MINUS}</OperationButton>}
        multiplicationButton={<OperationButton>{EOperations.MULTIPLICATION}</OperationButton>}
        plusButton={<OperationButton>{EOperations.PLUS}</OperationButton>}
      />
      <NumericPanelTemplate
        NumericButton={NumericButton}
        floatButton={<NumericButton>{FLOAT_CHARS.VISIBLE_CHAR}</NumericButton>}
      />
      <EqualAndResetPanelTemplate equalButton={<EqualButton />} resetButton={<ResetButton />} />
    </div>
  );
};
