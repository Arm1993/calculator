import { screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { renderWithProviders } from 'shared/test-helpers';
import { FLOAT_CHARS } from 'shared/model';
import { EOperations } from 'entities/panels';

import { Calculator } from '../calculator';

describe('panels', () => {
  let resultPanel: HTMLElement;
  let operationsPanel: HTMLElement;
  let numericPanel: HTMLElement;
  let equalAndResetPanel: HTMLElement;
  let resetButton: HTMLButtonElement;

  beforeEach(() => {
    renderWithProviders(<Calculator />);
    resultPanel = screen.getByTestId('ResultPanel');
    operationsPanel = screen.getByTestId('OperationsPanel');
    numericPanel = screen.getByTestId('NumericPanel');
    equalAndResetPanel = screen.getByTestId('EqualAndResetPanel');
    resetButton = within(equalAndResetPanel).getByText('C');
  });

  test('render', () => {
    expect(resultPanel).toBeInTheDocument();
    expect(operationsPanel).toBeInTheDocument();
    expect(numericPanel).toBeInTheDocument();
    expect(equalAndResetPanel).toBeInTheDocument();
  });

  describe('panels work with result panel', () => {
    let resultPanelTextArea: HTMLTextAreaElement;
    beforeEach(() => {
      resultPanelTextArea = within(resultPanel).getByRole('textbox');
    });

    test('operation panel', async () => {
      const operationPanelScreen = within(operationsPanel);
      const divisionButton = operationPanelScreen.getByText(EOperations.DIVISION);
      const multiplicationButton = operationPanelScreen.getByText(EOperations.MULTIPLICATION);
      const plusButton = operationPanelScreen.getByText(EOperations.PLUS);
      const minusButton = operationPanelScreen.getByText(EOperations.MINUS);

      expect(resultPanelTextArea).toBeEmptyDOMElement();
      await userEvent.click(divisionButton);
      expect(resultPanelTextArea).not.toHaveFocus();
      expect(resultPanelTextArea).toBeEmptyDOMElement();

      await userEvent.click(multiplicationButton);
      expect(resultPanelTextArea).not.toHaveFocus();
      expect(resultPanelTextArea).toBeEmptyDOMElement();

      await userEvent.click(plusButton);
      expect(resultPanelTextArea).not.toHaveFocus();
      expect(resultPanelTextArea).toBeEmptyDOMElement();

      await userEvent.click(minusButton);
      expect(resultPanelTextArea).toHaveFocus();
      expect(resultPanelTextArea).toHaveValue('-');

      await userEvent.click(document.body);
      expect(resultPanelTextArea).not.toHaveFocus();
    });

    test('numeric panel', async () => {
      const numericPanelScreen = within(numericPanel);
      const zeroButton = numericPanelScreen.getByText('0');
      const oneButton = numericPanelScreen.getByText('1');
      const twoButton = numericPanelScreen.getByText('2');
      const threeButton = numericPanelScreen.getByText('3');
      const fourButton = numericPanelScreen.getByText('4');
      const fiveButton = numericPanelScreen.getByText('5');
      const sixButton = numericPanelScreen.getByText('6');
      const sevenButton = numericPanelScreen.getByText('7');
      const eightButton = numericPanelScreen.getByText('8');
      const nineButton = numericPanelScreen.getByText('9');
      const floatButton = numericPanelScreen.getByText(FLOAT_CHARS.VISIBLE_CHAR);

      await userEvent.click(zeroButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(oneButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(twoButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(threeButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(fourButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(fiveButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(sixButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(sevenButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(eightButton);
      expect(resultPanelTextArea).toHaveFocus();
      await userEvent.click(nineButton);
      expect(resultPanelTextArea).toHaveFocus();

      expect(resultPanelTextArea).toHaveValue('0123456789');
      await userEvent.click(resetButton);

      await userEvent.click(floatButton);
      expect(resultPanelTextArea).toBeEmptyDOMElement();
      await userEvent.click(zeroButton);
      await userEvent.click(floatButton);
      await userEvent.click(floatButton);
      expect(resultPanelTextArea).not.toHaveFocus();
      expect(resultPanelTextArea).toHaveValue('0,');
    });

    test('equal and reset panel', async () => {
      const equalAndResetPanelScreen = within(equalAndResetPanel);
      const equalButton = equalAndResetPanelScreen.getByText('=');

      await userEvent.click(equalButton);
      expect(resultPanelTextArea).toBeEmptyDOMElement();
      await userEvent.type(resultPanelTextArea, '5');
      expect(resultPanelTextArea).toHaveValue('5');
      await userEvent.click(resetButton);
      expect(resultPanelTextArea).toBeEmptyDOMElement();
    });

    describe('all panels functionality', () => {
      const __withinOP = () => within(operationsPanel);
      const __withinNP = () => within(numericPanel);
      const __withinERP = () => within(equalAndResetPanel);

      let operationPanelScreen: ReturnType<typeof __withinOP>;
      let divisionButton: HTMLButtonElement;
      let multiplicationButton: HTMLButtonElement;
      let plusButton: HTMLButtonElement;
      let minusButton: HTMLButtonElement;

      let numericPanelScreen: ReturnType<typeof __withinNP>;
      let zeroButton: HTMLButtonElement;
      let oneButton: HTMLButtonElement;
      let twoButton: HTMLButtonElement;
      let threeButton: HTMLButtonElement;
      let fiveButton: HTMLButtonElement;
      let floatButton: HTMLButtonElement;

      let equalAndResetPanelScreen: ReturnType<typeof __withinERP>;
      let equalButton: HTMLButtonElement;
      beforeEach(() => {
        operationPanelScreen = within(operationsPanel);
        divisionButton = operationPanelScreen.getByText(EOperations.DIVISION);
        multiplicationButton = operationPanelScreen.getByText(EOperations.MULTIPLICATION);
        plusButton = operationPanelScreen.getByText(EOperations.PLUS);
        minusButton = operationPanelScreen.getByText(EOperations.MINUS);

        numericPanelScreen = within(numericPanel);
        zeroButton = numericPanelScreen.getByText('0');
        oneButton = numericPanelScreen.getByText('1');
        twoButton = numericPanelScreen.getByText('2');
        threeButton = numericPanelScreen.getByText('3');
        fiveButton = numericPanelScreen.getByText('5');
        floatButton = numericPanelScreen.getByText(FLOAT_CHARS.VISIBLE_CHAR);

        equalAndResetPanelScreen = within(equalAndResetPanel);
        equalButton = equalAndResetPanelScreen.getByText('=');
      });

      test('0.1 + 0.2 = 0.3', async () => {
        await userEvent.click(zeroButton);
        await userEvent.click(floatButton);
        await userEvent.click(oneButton);
        await userEvent.click(plusButton);
        await userEvent.click(zeroButton);
        await userEvent.click(floatButton);
        await userEvent.click(twoButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('0,3');
      });

      test('2.5 * 0.1 = 0.25', async () => {
        await userEvent.click(twoButton);
        await userEvent.click(floatButton);
        await userEvent.click(fiveButton);
        await userEvent.click(multiplicationButton);
        await userEvent.click(zeroButton);
        await userEvent.click(floatButton);
        await userEvent.click(oneButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('0,25');
      });

      test('5 / 2 = 2.5', async () => {
        await userEvent.click(fiveButton);
        await userEvent.click(divisionButton);
        await userEvent.click(twoButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('2,5');
      });

      test('-5 / 2 = -2.5', async () => {
        await userEvent.click(minusButton);
        await userEvent.click(fiveButton);
        await userEvent.click(divisionButton);
        await userEvent.click(twoButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('-2,5');
      });

      test('5 / -2 = -2.5', async () => {
        await userEvent.click(fiveButton);
        await userEvent.click(divisionButton);
        await userEvent.click(minusButton);
        await userEvent.click(twoButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('-2,5');
      });

      test('-5 / -2 = 2.5', async () => {
        await userEvent.click(minusButton);
        await userEvent.click(fiveButton);
        await userEvent.click(divisionButton);
        await userEvent.click(minusButton);
        await userEvent.click(twoButton);
        await userEvent.click(equalButton);

        expect(resultPanelTextArea).toHaveValue('2,5');
      });

      test('mixed', async () => {
        await userEvent.click(twoButton);
        expect(resultPanelTextArea).toHaveValue('2');
        await userEvent.click(plusButton);
        expect(resultPanelTextArea).toHaveValue('2+');
        await userEvent.click(threeButton);
        expect(resultPanelTextArea).toHaveValue('2+3');
        await userEvent.click(equalButton);
        expect(resultPanelTextArea).toHaveValue('5');

        await userEvent.click(divisionButton);
        await userEvent.click(twoButton);
        await userEvent.click(multiplicationButton);
        expect(resultPanelTextArea).toHaveValue('2,5x');
        await userEvent.click(twoButton);
        await userEvent.click(floatButton);
        await userEvent.click(twoButton);
        expect(resultPanelTextArea).toHaveValue('2,5x2,2');
        await userEvent.click(minusButton);
        expect(resultPanelTextArea).toHaveValue('5,5-');
        await userEvent.click(zeroButton);
        await userEvent.click(floatButton);
        await userEvent.click(fiveButton);
        await userEvent.click(minusButton);
        expect(resultPanelTextArea).toHaveValue('5-');
        await userEvent.click(fiveButton);
        await userEvent.click(minusButton);
        expect(resultPanelTextArea).toHaveValue('0-');
        await userEvent.click(fiveButton);
        await userEvent.click(minusButton);
        expect(resultPanelTextArea).toHaveValue('-5-');
        await userEvent.click(plusButton);
        await userEvent.click(fiveButton);
        await userEvent.click(equalButton);
        expect(resultPanelTextArea).toHaveValue('0');
        await userEvent.click(resetButton);
      });
    });
  });
});
