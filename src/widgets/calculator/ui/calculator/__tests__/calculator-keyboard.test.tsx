import { act, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { renderWithProviders } from 'shared/test-helpers/render-with-providers';
import { numpadNumbers, numpadKeyboardMap } from 'shared/test-helpers/numpad-keyboard-map';

import { Calculator } from '../calculator';

describe('panels working via keyboard', () => {
  let resultPanel: HTMLElement;
  let operationsPanel: HTMLElement;
  let numericPanel: HTMLElement;
  let equalAndResetPanel: HTMLElement;
  let resultPanelTextArea: HTMLTextAreaElement;

  beforeEach(() => {
    renderWithProviders(<Calculator />);
    resultPanel = screen.getByTestId('ResultPanel');
    operationsPanel = screen.getByTestId('OperationsPanel');
    numericPanel = screen.getByTestId('NumericPanel');
    equalAndResetPanel = screen.getByTestId('EqualAndResetPanel');
    resultPanelTextArea = within(resultPanel).getByRole('textbox');
    act(() => {
      resultPanelTextArea.focus();
    });
  });

  test('render', () => {
    expect(resultPanel).toBeInTheDocument();
    expect(operationsPanel).toBeInTheDocument();
    expect(numericPanel).toBeInTheDocument();
    expect(equalAndResetPanel).toBeInTheDocument();
    expect(resultPanelTextArea).toBeEmptyDOMElement();
  });

  test('focus/blur and focus before each test', () => {
    expect(resultPanelTextArea).toHaveFocus();
    act(() => {
      resultPanelTextArea.blur();
    });
    expect(resultPanelTextArea).not.toHaveFocus();
  });

  test('numpad', async () => {
    let codes = '';
    let keys = '';
    numpadNumbers.forEach(({ code, key }) => {
      codes += `[${code}]`;
      keys += key;
    });
    await userEvent.keyboard(codes, {
      keyboardMap: numpadKeyboardMap,
    });
    expect(resultPanelTextArea).toHaveValue(keys);
  });

  test('backSpace', async () => {
    await userEvent.type(resultPanelTextArea, '12.22+12', { keyboardMap: numpadKeyboardMap });
    expect(resultPanelTextArea).toHaveValue('12,22+12');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,22+1');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,22+');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,22');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,2');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('1');
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('');
  });

  // eslint-disable-next-line quotes
  test("backSpace don't break logic", async () => {
    await userEvent.type(resultPanelTextArea, '12.22+12', { keyboardMap: numpadKeyboardMap });
    await userEvent.keyboard('{backspace}');
    expect(resultPanelTextArea).toHaveValue('12,22+1');
    await userEvent.keyboard('1');
    await userEvent.keyboard('[NumpadEnter]');
    expect(resultPanelTextArea).toHaveValue('23,22');
  });

  //TODO: need complete
});
