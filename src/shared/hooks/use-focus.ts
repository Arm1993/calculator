import { useCallback, useState } from 'react';

interface IReturnValues {
  handleFocus: () => void;
  handleBlur: () => void;
  isFocus: boolean;
}

export const useFocus = (): IReturnValues => {
  const [isFocus, setIsFocus] = useState(false);

  const handleFocus = useCallback(() => void setIsFocus(true), []);
  const handleBlur = useCallback(() => void setIsFocus(false), []);

  return {
    handleFocus,
    handleBlur,
    isFocus,
  };
};
