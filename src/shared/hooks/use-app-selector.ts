import { TypedUseSelectorHook, useSelector } from 'react-redux';

import { TRootState } from 'shared/model';

export const useAppSelector: TypedUseSelectorHook<TRootState> = useSelector;
