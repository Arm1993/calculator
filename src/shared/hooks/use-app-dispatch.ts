import { useDispatch } from 'react-redux';

import { TDispatch } from 'shared/model';

export const useAppDispatch: () => TDispatch = useDispatch;
