/* eslint-disable @typescript-eslint/ban-types */
import { FC, ReactNode } from 'react';

import { store } from 'app';

export type TDispatch = typeof store.dispatch;
export type TRootState = ReturnType<typeof store.getState>;
export type TAction<T = void> = (
  arg: T,
) => (dispatch: TDispatch, getState: () => TRootState) => void;

export interface IElementProps {
  elementProps?: {
    'data-testid'?: string;
  };
}

export interface IChildren {
  children: ReactNode;
}

export interface IClassName {
  className?: string;
}

export type TFCChildren<T = {}> = FC<IChildren & T>;

export type TFCClassName<T = {}> = FC<IClassName & T>;

export type TFC<T = {}> = FC<IChildren & IClassName & T>;
