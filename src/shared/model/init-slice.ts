import { PayloadAction, createSlice } from '@reduxjs/toolkit';

interface IInitialState {
  isFontsReady: boolean;
}

const initialState: IInitialState = {
  isFontsReady: false,
};

export const initSlice = createSlice({
  initialState,
  name: 'preloadAsync',
  reducers: {
    setIsFontReady(state, { payload }: PayloadAction<boolean>) {
      state.isFontsReady = payload;
    },
  },
});

export const initSliceActions = initSlice.actions;
