import { ReactNode } from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import { setupStore } from 'app/store';

export const renderWithProviders = (
  element: ReactNode,
  commonStore?: ReturnType<typeof setupStore>,
): ReturnType<typeof render> => {
  if (!commonStore) {
    commonStore = setupStore();
  }
  return render(<Provider store={commonStore}>{element}</Provider>);
};
