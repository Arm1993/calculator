import { defaultKeyMap } from '@testing-library/user-event/dist/cjs/keyboard/keyMap.js';

export const numpadNumbers = new Array(10)
  .fill(null)
  .map((_, i) => ({ code: `Numpad${i}`, key: `${i}` }));

const additional = {
  NumLock: 'NumLock',
  NumpadAdd: '+',
  NumpadSubtract: '-',
  NumpadDivide: '/',
  NumpadMultiply: '*',
  NumpadDecimal: '.',
  NumpadEnter: 'Enter',
};

const numpadAdditional = Object.entries(additional).map(([key, value]) => {
  return {
    code: key,
    key: value,
  };
});

export const numpadKeyboardMap = [...numpadNumbers, ...numpadAdditional, ...defaultKeyMap];
