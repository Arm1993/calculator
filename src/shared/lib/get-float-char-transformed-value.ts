import { FLOAT_CHARS } from '../model/constants';

export const getFloatCharTransformedValue = (value: string, reverse = false): string => {
  const replaceValues: [string, string] = [FLOAT_CHARS.VISIBLE_CHAR, FLOAT_CHARS.REAL_CHAR];
  reverse && replaceValues.reverse();
  return value.replace(...replaceValues);
};
