import { getDate } from '../get-date';

describe('get-date', () => {
  test('with config => [y, m, d] should return "current full year/current month/ current day"', () => {
    const date = getDate({ config: ['y', 'm', 'd'] });
    const test_date = new Date();
    expect(date).toBe(
      `${test_date.getFullYear()}/${test_date.getMonth() + 1}/${test_date.getDate()}`,
    );
  });
});
