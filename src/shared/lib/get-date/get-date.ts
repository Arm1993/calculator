interface IArgs {
  config: ['y'?, 'm'?, 'd'?];
  divider?: '/';
}

type TKeys = Required<IArgs['config']>[number];

export const getDate = ({ config, divider = '/' }: IArgs): string => {
  const dateNow = new Date();
  const dates: Record<TKeys, number> = {
    y: dateNow.getFullYear(),
    m: dateNow.getMonth() + 1,
    d: dateNow.getDate(),
  };

  const date = config.map((key) => dates[key!]).join(divider);

  return date;
};
