import { createAsyncThunk } from '@reduxjs/toolkit';

import { TRootState, TDispatch } from 'shared/model';

export const createAppAsyncThunk = createAsyncThunk.withTypes<{
  state: TRootState;
  dispatch: TDispatch;
  rejectValue: string;
}>();
