import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const baseApi = createApi({
  endpoints: () => ({}),
  baseQuery: fetchBaseQuery({
    baseUrl: 'api',
  }),
  reducerPath: 'baseApi',
});
