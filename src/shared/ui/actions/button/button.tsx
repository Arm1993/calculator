import cn from 'classnames';
import { ComponentProps } from 'react';

import { TFC } from 'shared/model';

type TButtonProps = Pick<ComponentProps<'button'>, 'onClick'>;

export const Button: TFC<TButtonProps> = ({ children, onClick, className }) => {
  return (
    <button
      onClick={onClick}
      className={cn('font-intel', 'text-sm', 'font-medium', 'hover:text-lg', className)}
    >
      {children}
    </button>
  );
};
