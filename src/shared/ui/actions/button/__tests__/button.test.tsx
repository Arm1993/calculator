import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { Button } from '../button';

describe('button', () => {
  let button: HTMLButtonElement;
  let handleClick: jest.Mock;

  beforeEach(() => {
    handleClick = jest.fn();
    render(<Button onClick={handleClick}>123</Button>);
    button = screen.getByRole('button');
  });

  test('onClick', async () => {
    await userEvent.click(button);
    expect(handleClick).toBeCalledTimes(1);
  });

  test('value', async () => {
    expect(button).toHaveTextContent('123');
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
