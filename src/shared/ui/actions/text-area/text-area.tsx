import cn from 'classnames';
import { ComponentProps, forwardRef } from 'react';

import { IClassName } from 'shared/model';

type TProps = Pick<
  ComponentProps<'textarea'>,
  'value' | 'onChange' | 'onFocus' | 'onBlur' | 'onKeyDown'
> &
  IClassName;

export const TextArea = forwardRef<HTMLTextAreaElement, TProps>(
  ({ onChange, value, onFocus, onBlur, className, onKeyDown }, ref) => {
    return (
      <textarea
        ref={ref}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        onFocus={onFocus}
        onKeyDown={onKeyDown}
        className={cn(
          'border-white',
          'rounded-md',
          'solid',
          'border-2',
          'focus-within:border-gray',
          'font-intel',
          'text-sm',
          'font-medium',
          'outline-none',
          'resize-none',
          'bg-inherit',
          className,
        )}
      />
    );
  },
);

TextArea.displayName = 'TextArea';
