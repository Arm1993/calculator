import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { TextArea } from '../text-area';

describe('text-area', () => {
  let textArea: HTMLTextAreaElement;
  let handleChange: jest.Mock;
  let handleFocus: jest.Mock;
  let handleBlur: jest.Mock;

  beforeEach(() => {
    handleChange = jest.fn();
    handleFocus = jest.fn();
    handleBlur = jest.fn();
    render(
      <TextArea onChange={handleChange} value="123a" onFocus={handleFocus} onBlur={handleBlur} />,
    );
    textArea = screen.getByRole('textbox');
  });

  test('onChange', async () => {
    await userEvent.type(textArea, '123');
    expect(handleChange).toBeCalledTimes(3);
  });

  test('value', () => {
    expect(textArea).toHaveValue('123a');
  });

  test('focus/blur', async () => {
    render(<div data-testid="div"></div>);
    const div = screen.getByTestId('div');
    await userEvent.type(textArea, '123');
    expect(handleFocus).toBeCalledTimes(1);
    await userEvent.click(div);
    expect(handleBlur).toBeCalledTimes(1);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
