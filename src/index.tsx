import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';

import { store, App } from 'app';

import './index.css';

if ('serviceWorker' in navigator && !IS_DEVELOPMENT) {
  window.addEventListener('load', async () => {
    try {
      await navigator.serviceWorker.register('service-worker.js');
    } catch (err) {
      console.error('SW registration failed: ', err);
    }
  });
}

createRoot(document.getElementById('root') as HTMLElement).render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>,
);
