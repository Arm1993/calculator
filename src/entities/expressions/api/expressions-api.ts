import { baseApi } from 'shared/api/base-api';

import { RExpressionResponse } from '../model/types';

const baseApiWithTag = baseApi.enhanceEndpoints({
  addTagTypes: ['Expressions'],
});

export const expressionsApi = baseApiWithTag.injectEndpoints({
  endpoints: (builder) => ({
    getExpressions: builder.query<RExpressionResponse[], void>({
      query: () => 'expressions',
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ _id }) => ({ type: 'Expressions', id: _id } as const)),
              { type: 'Expressions', id: 'LIST' },
            ]
          : [{ type: 'Expressions', id: 'LIST' }],
    }),
    addExpression: builder.mutation<RExpressionResponse, { expression: string[]; date: string }>({
      query: (body) => ({
        url: 'expressions',
        method: 'PUT',
        body,
      }),
      invalidatesTags: (_, error) => {
        return error ? [] : [{ type: 'Expressions', id: 'LIST' }];
      },
    }),
  }),
});

export const { useGetExpressionsQuery } = expressionsApi;
