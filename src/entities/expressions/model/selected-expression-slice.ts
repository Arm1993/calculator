import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { TRootState } from 'shared/model';

interface IInitialState {
  selectedId: string;
}

const initialState: IInitialState = {
  selectedId: '',
};

export const selectedExpressionSlice = createSlice({
  name: 'selectedExpressionSlice',
  initialState,
  reducers: {
    setSelectedId(state, { payload }: PayloadAction<string>) {
      state.selectedId = payload;
    },
  },
});

export const selectedExpressionSliceSelectors = {
  selectSelectedId: (state: TRootState) => state.selectedExpressionSlice.selectedId,
};

export const selectedExpressionSliceActions = selectedExpressionSlice.actions;
