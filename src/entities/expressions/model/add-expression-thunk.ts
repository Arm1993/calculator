import { expressionsApi } from 'entities/expressions';
import { getDate } from 'shared/lib';
import { createAppAsyncThunk } from 'shared/lib/create-app-async-thunk';

export const addExpressionThunk = createAppAsyncThunk(
  'expressions/addExpression',
  async (expression: string[], { dispatch }) => {
    const date = getDate({ config: ['y', 'm', 'd'] });

    return dispatch(
      expressionsApi.endpoints.addExpression.initiate({
        date,
        expression,
      }),
    );
  },
);
