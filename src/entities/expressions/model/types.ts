export interface IExpression {
  _id: string;
  expression: string[];
}

export interface RExpressionResponse {
  _id: string;
  date: string;
  expressions: IExpression[];
}

export interface IInitialState {
  expressionsStore: RExpressionResponse[];
  selectedExpressionId: RExpressionResponse['_id'];
}
