import { getFloatCharTransformedValue } from 'shared/lib';

interface IArgs {
  leftOperand: string;
  operation: string;
  rightOperand: string;
}

export const getValidExpression = ({ leftOperand, operation, rightOperand }: IArgs): string[] => {
  const left = getFloatCharTransformedValue(leftOperand);
  const right = getFloatCharTransformedValue(rightOperand);
  return [left, operation, right];
};
