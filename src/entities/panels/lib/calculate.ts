import { EOperations, IPanelSliceInitialState, FLOAT_CHARS } from 'entities/panels';
import { getFloatCharTransformedValue } from 'shared/lib';

export const calculate = ({
  firstStringifyNumber,
  secondStringifyNumber,
  operation,
}: Pick<
  IPanelSliceInitialState,
  'firstStringifyNumber' | 'secondStringifyNumber' | 'operation'
>): string => {
  const firstNumber = parseFloat(getFloatCharTransformedValue(firstStringifyNumber));
  const secondNumber = parseFloat(getFloatCharTransformedValue(secondStringifyNumber));

  const a = firstStringifyNumber.match(/,.+/)?.[0].length ?? 0;
  const b = secondStringifyNumber.match(/,.+/)?.[0].length ?? 0;

  const res = a + b;

  const length = a && b ? 2 : 1;

  const afterDotCharsCount = res > length ? res - length : 0;

  const getValidValue = (value: number): string => {
    if (afterDotCharsCount) {
      if (value.toString().includes(FLOAT_CHARS.REAL_CHAR)) {
        return value.toFixed(afterDotCharsCount).replace(/0$/, '');
      }
    }
    return value.toString();
  };

  switch (operation) {
    case EOperations.PLUS:
      return getFloatCharTransformedValue(getValidValue(firstNumber + secondNumber), true);
    case EOperations.MINUS:
      return getFloatCharTransformedValue(getValidValue(firstNumber - secondNumber), true);
    case EOperations.DIVISION:
      return getFloatCharTransformedValue(getValidValue(firstNumber / secondNumber), true);
    case EOperations.MULTIPLICATION:
      return getFloatCharTransformedValue(getValidValue(firstNumber * secondNumber), true);
    default:
      throw Error('invalid operation');
  }
};
