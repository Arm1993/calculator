import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { TRootState } from 'shared/model';

import { EOperations, IPanelSliceInitialState } from './types';

const initialState: IPanelSliceInitialState = {
  firstStringifyNumber: '',
  secondStringifyNumber: '',
  operation: '',
};

export const panelSlice = createSlice({
  name: 'panels',
  initialState,
  reducers: {
    setFirstNumber(state, { payload }: PayloadAction<string>) {
      state.firstStringifyNumber += payload;
    },
    resetFirstNumber(
      state,
      { payload = initialState.firstStringifyNumber }: PayloadAction<string | undefined>,
    ) {
      state.firstStringifyNumber = payload;
    },
    setSecondNumber(state, { payload }: PayloadAction<string>) {
      state.secondStringifyNumber += payload;
    },
    resetSecondNumber(
      state,
      { payload = initialState.secondStringifyNumber }: PayloadAction<string | undefined>,
    ) {
      state.secondStringifyNumber = payload;
    },
    setOperation(state, { payload }: PayloadAction<EOperations>) {
      state.operation = payload;
    },
    resetOperation(state) {
      state.operation = initialState.operation;
    },
    setResult(state, { payload }: PayloadAction<string>) {
      state.firstStringifyNumber = payload;
    },
    reset() {
      return initialState;
    },
  },
});

export const panelSliceActions = panelSlice.actions;

export const panelSliceSelectors = {
  selectFirstStringifyNumber: (state: TRootState) => state.panels.firstStringifyNumber,
  selectSecondStringifyNumber: (state: TRootState) => state.panels.secondStringifyNumber,
  selectOperation: (state: TRootState) => state.panels.operation,
};
