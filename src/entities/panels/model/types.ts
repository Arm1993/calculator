export enum EOperations {
  DIVISION = '/',
  MULTIPLICATION = 'x',
  MINUS = '-',
  PLUS = '+',
}

export interface IPanelSliceInitialState {
  firstStringifyNumber: string;
  secondStringifyNumber: string;
  operation: EOperations | '';
}
