import { FC } from 'react';
import cn from 'classnames';

import { PanelColumn } from '../panel-column/panel-column';
import { PanelRow } from '../panel-row/panel-row';

const panelColumnClassNames = cn(
  'flex',
  'items-center',
  'justify-center',
  'border',
  'border-solid',
  'border-gray',
  'basis-1/4',
);

interface IOperationsPanelProps {
  divisionButton: JSX.Element;
  multiplicationButton: JSX.Element;
  minusButton: JSX.Element;
  plusButton: JSX.Element;
}

export const OperationsPanelTemplate: FC<IOperationsPanelProps> = ({
  divisionButton,
  minusButton,
  multiplicationButton,
  plusButton,
}) => {
  return (
    <PanelRow
      elementProps={{
        'data-testid': 'OperationsPanel',
      }}
      className="flex gap-2"
    >
      <PanelColumn className={panelColumnClassNames}>{divisionButton}</PanelColumn>
      <PanelColumn className={panelColumnClassNames}>{multiplicationButton}</PanelColumn>
      <PanelColumn className={panelColumnClassNames}>{minusButton}</PanelColumn>
      <PanelColumn className={panelColumnClassNames}>{plusButton}</PanelColumn>
    </PanelRow>
  );
};
