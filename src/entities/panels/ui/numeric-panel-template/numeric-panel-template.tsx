import { FC } from 'react';
import cn from 'classnames';

import { PanelColumn } from '../panel-column/panel-column';
import { PanelRow } from '../panel-row/panel-row';

import styles from './numeric-panel.module.scss';

const numericValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] as const;

const classNames = cn(
  'flex',
  'items-center',
  'justify-center',
  'border',
  'border-gray',
  'border-solid',
);

interface INumericPanelProps {
  NumericButton: FC<{ children: number | string }>;
  floatButton: JSX.Element;
}

export const NumericPanelTemplate: FC<INumericPanelProps> = ({ floatButton, NumericButton }) => {
  return (
    <PanelRow
      elementProps={{
        'data-testid': 'NumericPanel',
      }}
      className={cn(styles.gridTemplateAreas, 'grid', 'gap-2', 'grid-cols-3')}
    >
      {numericValues.map((numericValue) => (
        <PanelColumn
          className={cn(styles[`grid-area-grid-${numericValue}`], classNames)}
          key={numericValue}
        >
          <NumericButton>{numericValue}</NumericButton>
        </PanelColumn>
      ))}
      <PanelColumn className={cn(styles['grid-area-grid-dot'], classNames)}>
        {floatButton}
      </PanelColumn>
    </PanelRow>
  );
};
