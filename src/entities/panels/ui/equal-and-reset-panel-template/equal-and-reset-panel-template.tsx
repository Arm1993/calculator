import { FC } from 'react';

import { PanelColumn } from '../panel-column/panel-column';
import { PanelRow } from '../panel-row/panel-row';

interface IEqualAndResetPanelProps {
  equalButton: JSX.Element;
  resetButton: JSX.Element;
}

export const EqualAndResetPanelTemplate: FC<IEqualAndResetPanelProps> = ({
  equalButton,
  resetButton,
}) => {
  return (
    <PanelRow
      elementProps={{
        'data-testid': 'EqualAndResetPanel',
      }}
      className="grid-cols-equal-and-reset-panel-row gap-2 grid"
    >
      <PanelColumn className="bg-dark-blue">{equalButton}</PanelColumn>
      <PanelColumn>{resetButton}</PanelColumn>
    </PanelRow>
  );
};
