import { TFCChildren } from 'shared/model';

import { PanelColumn } from '../panel-column/panel-column';
import { PanelRow } from '../panel-row/panel-row';

export const ResultPanelTemplate: TFCChildren = ({ children }) => {
  return (
    <PanelRow
      elementProps={{
        'data-testid': 'ResultPanel',
      }}
    >
      <PanelColumn className="flex items-center justify-end bg-light-gray">{children}</PanelColumn>
    </PanelRow>
  );
};
