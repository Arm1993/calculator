import cn from 'classnames';

import { IElementProps, TFC } from 'shared/model';

export const PanelColumn: TFC<IElementProps> = ({ children, className, elementProps }) => {
  return (
    <div {...elementProps} className={cn('h-[52px]', 'rounded-md', 'flex', 'grow', className)}>
      {children}
    </div>
  );
};
