export { EqualAndResetPanelTemplate } from './equal-and-reset-panel-template';
export { NumericPanelTemplate } from './numeric-panel-template';
export { OperationsPanelTemplate } from './operations-panel-template';
export { ResultPanelTemplate } from './result-panel-template';
