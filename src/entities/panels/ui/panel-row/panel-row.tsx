import cn from 'classnames';

import { IElementProps, TFC } from 'shared/model';

export const PanelRow: TFC<IElementProps> = ({ children, className, elementProps }) => {
  return (
    <div
      {...elementProps}
      className={cn('w-[240px]', 'rounded', 'p-1', 'shadow-light-gray', className)}
    >
      {children}
    </div>
  );
};
