import { FC, MouseEventHandler } from 'react';

import { EOperations } from 'entities/panels';
import { useAppDispatch } from 'shared/hooks';
import { Button } from 'shared/ui';

import { setNumberOrOperationThunk } from '../model/set-number-or-operation';

export const OperationButton: FC<{ children: EOperations }> = ({ children }) => {
  const dispatch = useAppDispatch();
  const handler: MouseEventHandler<HTMLButtonElement> = (e) => {
    dispatch(setNumberOrOperationThunk(e.currentTarget.innerHTML as EOperations));
  };
  return <Button onClick={handler}>{children}</Button>;
};
