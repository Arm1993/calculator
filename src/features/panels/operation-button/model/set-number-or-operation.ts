import {
  EOperations,
  IPanelSliceInitialState,
  panelSliceActions,
  calculate,
} from 'entities/panels';
import { addExpressionThunk, getValidExpression } from 'entities/expressions';
import { TAction } from 'shared/model';

const { setFirstNumber, setSecondNumber, setOperation, setResult, resetSecondNumber } =
  panelSliceActions;

type TSetOperationResultArgs = Required<
  Pick<IPanelSliceInitialState, 'firstStringifyNumber' | 'operation' | 'secondStringifyNumber'>
>;

const setOperationResult: TAction<TSetOperationResultArgs> =
  ({ firstStringifyNumber, operation, secondStringifyNumber }) =>
  async (dispatch) => {
    const expression = getValidExpression({
      leftOperand: firstStringifyNumber,
      operation,
      rightOperand: secondStringifyNumber,
    });
    const { payload } = await dispatch(addExpressionThunk(expression));

    if (payload && typeof payload !== 'string' && 'error' in payload) {
      return;
    }
    const result = calculate({ firstStringifyNumber, operation, secondStringifyNumber });
    dispatch(setResult(result));
    dispatch(resetSecondNumber());
  };

export const setNumberOrOperationThunk: TAction<EOperations> = (operationPanelValue) => {
  return (dispatch, getState) => {
    const panelsState = getState().panels;
    const { firstStringifyNumber, operation, secondStringifyNumber } = panelsState;
    if (operationPanelValue === EOperations.MINUS && !firstStringifyNumber) {
      dispatch(setFirstNumber(operationPanelValue));
    } else if (firstStringifyNumber !== EOperations.MINUS) {
      const isReady = firstStringifyNumber && operation && secondStringifyNumber;
      if (isReady) {
        dispatch(setOperationResult({ firstStringifyNumber, operation, secondStringifyNumber }));
      }
      if (firstStringifyNumber) {
        if (operationPanelValue === EOperations.MINUS && operation && !isReady) {
          dispatch(setSecondNumber(operationPanelValue));
        } else {
          dispatch(setOperation(operationPanelValue));
        }
      }
    }
  };
};
