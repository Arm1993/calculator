import { FC, useEffect, useRef } from 'react';
import cn from 'classnames';

import { TextArea } from 'shared/ui';
import { useFocus } from 'shared/hooks';

import { useStore } from '../hooks/use-store';

export const ResultPanel: FC = () => {
  const { input, handleChange, handleKeyDown } = useStore();

  const ref = useRef<HTMLTextAreaElement>(null);
  const { handleBlur, handleFocus, isFocus } = useFocus();

  useEffect(() => {
    if (input && !isFocus) {
      ref.current!.focus();
    }
  }, [input]);

  return (
    <TextArea
      onChange={handleChange}
      onBlur={handleBlur}
      onFocus={handleFocus}
      onKeyDown={handleKeyDown}
      value={input}
      ref={ref}
      className={cn('w-full', 'h-full', 'font-bold', 'py-0', 'px-2', 'text-lg')}
    />
  );
};
