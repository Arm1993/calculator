import { ChangeEvent, KeyboardEvent } from 'react';

import { panelSliceSelectors, panelSliceActions } from 'entities/panels';
import { FLOAT_CHARS } from 'shared/model/constants';
import { EOperations } from 'entities/panels';
import { setNumberOrOperationThunk } from 'features/panels/operation-button/model/set-number-or-operation';
import { useAppSelector, useAppDispatch } from 'shared/hooks';
import { setNumberThunk } from 'features/panels/numeric-button/model/set-number';
import { setEqualResultThunk } from 'features/panels/equal-button/model/set-equal-result';

import { removeLastCharacter } from '../model/remove-last-character';

type THandleChange = (e: ChangeEvent<HTMLTextAreaElement>) => void;
type TOnKeyDown = (e: KeyboardEvent<HTMLTextAreaElement>) => void;

const { selectFirstStringifyNumber, selectOperation, selectSecondStringifyNumber } =
  panelSliceSelectors;

interface IReturnValues {
  input: string;
  handleChange: THandleChange;
  handleKeyDown: TOnKeyDown;
}

const { reset } = panelSliceActions;

export const useStore = (): IReturnValues => {
  const firstStringifyNumber = useAppSelector(selectFirstStringifyNumber);
  const secondStringifyNumber = useAppSelector(selectSecondStringifyNumber);
  const operation = useAppSelector(selectOperation);

  const dispatch = useAppDispatch();

  const handleChange: THandleChange = (e) => {
    const lastInput = e.target.value[e.target.value.length - 1];
    if (!Number.isNaN(parseInt(lastInput))) {
      dispatch(setNumberThunk(lastInput));
    }
  };

  const input = [firstStringifyNumber, operation, secondStringifyNumber].filter(Boolean).join('');

  const handleKeyDown: TOnKeyDown = (e) => {
    switch (e.code) {
      case 'Digit0':
      case 'Digit1':
      case 'Digit2':
      case 'Digit3':
      case 'Digit4':
      case 'Digit5':
      case 'Digit6':
      case 'Digit7':
      case 'Digit8':
      case 'Digit9':
      case 'Numpad0':
      case 'Numpad1':
      case 'Numpad2':
      case 'Numpad3':
      case 'Numpad4':
      case 'Numpad5':
      case 'Numpad6':
      case 'Numpad7':
      case 'Numpad8':
      case 'Numpad9':
        e.preventDefault();
        dispatch(setNumberThunk(e.key));
        break;

      case 'NumpadAdd':
      case 'NumpadSubtract':
      case 'NumpadDivide':
        e.preventDefault();
        dispatch(setNumberOrOperationThunk(e.key as EOperations));
        break;
      case 'NumpadMultiply':
        e.preventDefault();
        dispatch(setNumberOrOperationThunk(EOperations.MULTIPLICATION));
        break;
      case 'NumpadDecimal':
        e.preventDefault();
        dispatch(setNumberThunk(FLOAT_CHARS.VISIBLE_CHAR));
        break;
      case 'NumpadEnter':
        e.preventDefault();
        dispatch(setEqualResultThunk());
        break;
      case 'Backspace':
        e.preventDefault();
        dispatch(removeLastCharacter());
        break;
      case 'Delete':
        e.preventDefault();
        dispatch(reset());
        break;
      default:
        break;
    }
  };

  return {
    input,
    handleChange,
    handleKeyDown,
  };
};
