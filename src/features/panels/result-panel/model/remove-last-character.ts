import { panelSliceActions } from 'entities/panels';
import { TAction } from 'shared/model';

const { resetSecondNumber, resetOperation, resetFirstNumber } = panelSliceActions;

export const removeLastCharacter: TAction = () => (dispatch, getState) => {
  const { firstStringifyNumber, operation, secondStringifyNumber } = getState().panels;
  if (operation) {
    if (secondStringifyNumber) {
      const updatedSecondStringifyNumber = secondStringifyNumber.slice(
        0,
        secondStringifyNumber.length - 1,
      );
      dispatch(resetSecondNumber(updatedSecondStringifyNumber));
    } else {
      dispatch(resetOperation());
    }
  } else {
    if (firstStringifyNumber) {
      const updatedFirsStringifyNumber = firstStringifyNumber.slice(
        0,
        firstStringifyNumber.length - 1,
      );
      dispatch(resetFirstNumber(updatedFirsStringifyNumber));
    }
  }
};
