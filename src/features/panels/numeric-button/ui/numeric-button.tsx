import { FC, MouseEventHandler } from 'react';

import { useAppDispatch } from 'shared/hooks/use-app-dispatch';
import { TFCChildren } from 'shared/model';
import { Button } from 'shared/ui';

import { setNumberThunk } from '../model/set-number';

export const NumericButton: FC<{ children: number | string }> = ({ children }) => {
  const dispatch = useAppDispatch();
  const handler: MouseEventHandler<HTMLButtonElement> = (e) => {
    dispatch(setNumberThunk(e.currentTarget.innerHTML));
  };
  return <Button onClick={handler}>{children}</Button>;
};
