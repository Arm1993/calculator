import { FLOAT_CHARS } from 'entities/panels';
import { panelSliceActions } from 'entities/panels';
import { TAction } from 'shared/model';

const { setFirstNumber, setSecondNumber } = panelSliceActions;

export const setNumberThunk: TAction<string> = (numericPanelValue) => {
  return (dispatch, getState) => {
    const { firstStringifyNumber, operation, secondStringifyNumber } = getState().panels;
    if (operation) {
      if (numericPanelValue === FLOAT_CHARS.VISIBLE_CHAR) {
        if (secondStringifyNumber && !secondStringifyNumber.includes(FLOAT_CHARS.VISIBLE_CHAR)) {
          dispatch(setSecondNumber(numericPanelValue));
        }
      } else {
        dispatch(setSecondNumber(numericPanelValue));
      }
    } else {
      if (numericPanelValue === FLOAT_CHARS.VISIBLE_CHAR) {
        if (firstStringifyNumber && !firstStringifyNumber.includes(FLOAT_CHARS.VISIBLE_CHAR)) {
          dispatch(setFirstNumber(numericPanelValue));
        }
      } else {
        dispatch(setFirstNumber(numericPanelValue));
      }
    }
  };
};
