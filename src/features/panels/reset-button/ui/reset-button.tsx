import { FC } from 'react';

import { panelSliceActions } from 'entities/panels';
import { useAppDispatch } from 'shared/hooks';
import { Button } from 'shared/ui';

const { reset } = panelSliceActions;

export const ResetButton: FC = () => {
  const dispatch = useAppDispatch();
  const handleReset = () => {
    dispatch(reset());
  };
  return (
    <Button onClick={handleReset} className="border-gray border-solid border rounded-md">
      C
    </Button>
  );
};
