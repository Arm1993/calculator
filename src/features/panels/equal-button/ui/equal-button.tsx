import { FC } from 'react';

import { Button } from 'shared/ui/actions/button/button';
import { useAppDispatch } from 'shared/hooks';

import { setEqualResultThunk } from '../model/set-equal-result';

export const EqualButton: FC = () => {
  const dispatch = useAppDispatch();
  const handleEqual = () => {
    dispatch(setEqualResultThunk());
  };

  return (
    <Button className="text-white" onClick={handleEqual}>
      =
    </Button>
  );
};
