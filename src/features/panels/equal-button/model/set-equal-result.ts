import { panelSliceActions, calculate } from 'entities/panels';
import { getValidExpression, addExpressionThunk } from 'entities/expressions';
import { TAction } from 'shared/model';

const { resetSecondNumber, resetOperation, setResult } = panelSliceActions;

export const setEqualResultThunk: TAction = () => {
  return async (dispatch, getState) => {
    const { firstStringifyNumber, operation, secondStringifyNumber } = getState().panels;
    const isReady = firstStringifyNumber && operation && secondStringifyNumber;
    if (isReady) {
      const expression = getValidExpression({
        leftOperand: firstStringifyNumber,
        operation,
        rightOperand: secondStringifyNumber,
      });
      const { payload } = await dispatch(addExpressionThunk(expression));

      if (payload && typeof payload !== 'string' && 'error' in payload) {
        return;
      }
      const result = calculate({ firstStringifyNumber, operation, secondStringifyNumber });
      dispatch(setResult(result));
      dispatch(resetSecondNumber());
      dispatch(resetOperation());
    }
  };
};
