import { TypedUseQueryHookResult } from '@reduxjs/toolkit/query/react';
import cn from 'classnames';
import { FC, memo } from 'react';

import { TFCChildren } from 'shared/model';
import { Button } from 'shared/ui';

interface IProps {
  w: `w-[${string}]`;
  h: `h-[${string}]`;
}
type TProps = IProps & TypedUseQueryHookResult<any, any, any>;

const Component: TFCChildren<TProps> = ({
  children,
  w = 'w-[300px]',
  h = 'h-[300px]',
  isError,
  isLoading,
  isSuccess,
  refetch,
}) => {
  const blockGap = 20;

  return (
    <div className={cn(w, h, 'overflow-auto')}>
      {isLoading && (
        <div className="border shadow rounded-md p-4 max-w-sm w-full mx-auto">
          <div className={cn(`animate-pulse flex gap-[${blockGap}px] flex-col ${w}`)}>
            <Lines h={h} gap={blockGap} />
          </div>
        </div>
      )}
      {isSuccess && children}
      {isError && (
        <div className="w-full h-full flex flex-col justify-center items-center border shadow rounded-md p-4">
          <span>An error has occurred</span>
          {refetch && (
            <>
              <span>please reload this window.</span>
              <Button
                onClick={() => {
                  refetch();
                }}
                className="mt-2 flex items-center justify-center w-[80px] h-[40px] p-3 bg-light-gray border-solid rounded-md border-gray"
              >
                reload
              </Button>
            </>
          )}
        </div>
      )}
    </div>
  );
};

const Lines: FC<{ gap: number } & Pick<IProps, 'h'>> = memo(({ h, gap }) => {
  const blockHeight = 20;
  const height = h.match(/\d+/g)![0];
  const lineCount = Math.floor(+height / (blockHeight + gap));

  return (
    <>
      {new Array(lineCount).fill(null).map((_, i) => (
        <div
          key={i}
          style={{
            height: blockHeight,
          }}
          className={cn('w-full', 'bg-slate-300', 'rounded-md')}
        />
      ))}
    </>
  );
});

Lines.displayName = 'Lines';

export const ValidateQuery = memo(
  Component,
  (prev, next) =>
    prev.isLoading === next.isLoading &&
    prev.isError === next.isError &&
    prev.isSuccess === next.isSuccess,
);
