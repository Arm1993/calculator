import { FC, memo } from 'react';

import { selectedExpressionSliceActions } from 'entities/expressions';
import { useAppDispatch } from 'shared/hooks';
import { Button } from 'shared/ui';

const { setSelectedId } = selectedExpressionSliceActions;

interface IProps {
  children: string;
  id: string;
  className: string;
}

export const SelectExpressionButton: FC<IProps> = memo(({ children, id, className }) => {
  const dispatch = useAppDispatch();
  return (
    <Button
      className={className}
      onClick={() => {
        dispatch(setSelectedId(id));
      }}
    >
      {children}
    </Button>
  );
});
