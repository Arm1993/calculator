import { configureStore } from '@reduxjs/toolkit';

import { selectedExpressionSlice } from 'entities/expressions';
import { panelSlice } from 'entities/panels';
import { baseApi } from 'shared/api';
import { initSlice } from 'shared/model';

export const setupStore = () => {
  return configureStore({
    reducer: {
      [panelSlice.name]: panelSlice.reducer,
      [initSlice.name]: initSlice.reducer,
      [selectedExpressionSlice.name]: selectedExpressionSlice.reducer,
      [baseApi.reducerPath]: baseApi.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(baseApi.middleware),
  });
};

export const store = setupStore();
