import { FC } from 'react';

import { Expressions } from 'widgets/expressions';
import { Calculator } from 'widgets/calculator';

import { useInit } from './use-init';

export const App: FC = () => {
  useInit();

  return (
    <div className="flex items-center justify-center h-screen">
      <div className="flex gap-[20px] h-[456px]">
        <Calculator />
        <Expressions />
      </div>
    </div>
  );
};
