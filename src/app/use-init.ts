import { useEffect } from 'react';

import { useAppDispatch } from 'shared/hooks';
import { initSliceActions } from 'shared/model/init-slice';

export const useInit = () => {
  const dispatch = useAppDispatch();
  const { setIsFontReady } = initSliceActions;

  useEffect(() => {
    const init = async () => {
      await document.fonts.ready;
      dispatch(setIsFontReady(true));
    };
    init();
  }, []);
};
