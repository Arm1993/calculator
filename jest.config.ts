import { getGlobalVariables } from './webpack/constants';

import type { Config } from 'jest';

const config: Config = {
  preset: 'ts-jest',
  roots: ['<rootDir>/src'],
  testMatch: ['**/__tests__/**/*.+(ts|tsx|js)', '**/?(*.)+(spec|test).+(ts|tsx|js)'],
  transform: {
    '^.+\\.(ts|tsx)$': ['ts-jest', { tsconfig: './tsconfig.json' }],
  },
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: ['<rootDir>/jest/jest-setup.ts'],
  moduleNameMapper: {
    '\\.svg$': '<rootDir>/jest/jest-svgr.ts',
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  modulePaths: ['<rootDir>/src'],
  globals: getGlobalVariables('development'),
  reporters: ['default', 'jest-stare'],
};

export default config;
