declare const IS_DEVELOPMENT: boolean;

declare module '*.module.scss' {
  const styles: Record<string, string>;
  export default styles;
}

declare module '*.svg' {
  import { FC, SVGProps } from 'react';

  export const ReactComponent: FC<SVGProps<SVGSVGElement>>;
}
