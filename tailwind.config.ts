import { Config } from 'tailwindcss';

const config: Config = {
  content: ['src/**/*.tsx'],
  theme: {
    extend: {
      gridTemplateColumns: {
        'equal-and-reset-panel-row': '150px 1fr',
      },
      colors: {
        gray: '#E2E3E5',
      },
      backgroundColor: {
        'light-gray': '#f3f4f6',
        'dark-blue': '#5D5FEF',
      },
      boxShadow: {
        'light-gray': '0 2px 4px rgb(0 0 0 / 6%), 0 4px 6px rgb(0 0 0 / 10%)',
      },
      fontFamily: {
        intel: ['intel'],
      },
    },
  },
  plugins: [],
};

export default config;
