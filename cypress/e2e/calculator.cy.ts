describe('testing calculate', () => {
  it('calculate 1 + 2 should give 3', () => {
    cy.visit('/');
    cy.get('[data-cyid=panels]').then(($panels) => {
      cy.get('textarea', { withinSubject: $panels }).then(($textArea) => {
        cy.get('button', {
          withinSubject: $panels,
        })
          .contains(1)
          .click();
        cy.wrap($textArea).should('have.value', '1');

        cy.get('button', { withinSubject: $panels }).contains('+').click();
        cy.wrap($textArea).should('have.value', '1+');

        cy.get('button', { withinSubject: $panels }).contains('2').click();
        cy.wrap($textArea).should('have.value', '1+2');

        cy.get('button', { withinSubject: $panels }).contains('=').click();
        cy.wrap($textArea).should('have.value', '3');
      });
    });
  });
});
