import path from 'path';
import fs from 'fs';

import { TMode } from './interfaces';

export const PORT = '9000';

const ROOT_DIRECTORY = fs.realpathSync(process.cwd());

const resolve = (relativePath: string) => path.resolve(ROOT_DIRECTORY, relativePath);

export const PATHS = {
  OUTPUT: resolve('build'),
  ENTRY: resolve('src/index.tsx'),
  HTML: resolve('src/index.html'),
  STYLELINT_CONFIG: resolve('.stylelintrc.json'),
  MANIFEST_FOLDER: resolve('src/app/manifest'),
};

export const getGlobalVariables = (mode: TMode) => ({
  IS_DEVELOPMENT: mode === 'development',
});
