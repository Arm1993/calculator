import HtmlWebpackPlugin from 'html-webpack-plugin';
import { DefinePlugin, WebpackPluginInstance } from 'webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import StylelintWebpackPlugin from 'stylelint-webpack-plugin';
// eslint-disable-next-line import/default
import CopyPlugin from 'copy-webpack-plugin';
import { GenerateSW } from 'workbox-webpack-plugin';
import ESLintPlugin from 'eslint-webpack-plugin';

import { getGlobalVariables, PATHS } from './constants';
import { TMode } from './interfaces';

export const getHtmlWebpackPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new HtmlWebpackPlugin({
    path: PATHS.OUTPUT,
    filename: 'index.html',
    template: PATHS.HTML,
    ...(mode === 'production' && {
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
  });
};

export const getDefinePluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new DefinePlugin(getGlobalVariables(mode));
};

export const getBundleAnalyzerPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new BundleAnalyzerPlugin();
};

export const getMiniCssExtractPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new MiniCssExtractPlugin();
};

export const getForkTsCheckerWebpackPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new ForkTsCheckerWebpackPlugin({
    async: mode === 'development',
    typescript: {
      memoryLimit: 4000,
    },
  });
};

export const getStylelintWebpackPlugin = (mode: TMode): WebpackPluginInstance => {
  return new StylelintWebpackPlugin({
    configFile: PATHS.STYLELINT_CONFIG,
  });
};

export const getCopyPluginInstance = (): WebpackPluginInstance => {
  return new CopyPlugin({
    patterns: [
      {
        from: PATHS.MANIFEST_FOLDER,
        to: PATHS.OUTPUT,
      },
    ],
  });
};

export const getWorkboxPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new GenerateSW({
    clientsClaim: true,
    skipWaiting: true,
    cleanupOutdatedCaches: true,
    mode,
  });
};

export const getESLintPluginInstance = (mode: TMode): WebpackPluginInstance => {
  return new ESLintPlugin({
    fix: false,
    extensions: ['ts', 'tsx'],
    failOnWarning: false,
    emitWarning: false,
  });
};
