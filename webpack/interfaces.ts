import { RuleSetRule, Configuration } from 'webpack';
import { Configuration as IDevServerConfiguration } from 'webpack-dev-server';

export interface IRule extends RuleSetRule {
  type?: 'asset/resource' | 'asset/inline' | 'asset/source' | 'asset';
}

export type TMode = 'development' | 'production';

type TDevTool =
  | 'eval'
  | 'eval-cheap-source-map'
  | 'eval-cheap-module-source-map'
  | 'eval-source-map'
  | 'cheap-source-map'
  | 'cheap-module-source-map'
  | 'source-map'
  | 'inline-cheap-source-map'
  | 'inline-cheap-module-source-map'
  | 'inline-source-map'
  | 'eval-nosources-cheap-source-map'
  | 'eval-nosources-cheap-module-source-map'
  | 'eval-nosources-source-map'
  | 'inline-nosources-cheap-source-map'
  | 'inline-nosources-cheap-module-source-map'
  | 'inline-nosources-source-map'
  | 'nosources-cheap-source-map'
  | 'nosources-cheap-module-source-map'
  | 'nosources-source-map'
  | 'hidden-nosources-cheap-source-map'
  | 'hidden-nosources-cheap-module-source-map'
  | 'hidden-nosources-source-map'
  | 'hidden-cheap-source-map'
  | 'hidden-cheap-module-source-map'
  | 'hidden-source-map';

export interface IModifiedConfiguration extends Configuration {
  devtool?: TDevTool;
  devServer?: IDevServerConfiguration;
  module?: Configuration['module'] & {
    rules: IRule[];
  };
}
