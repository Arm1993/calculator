import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import { Configuration } from 'webpack';

import { PATHS } from './constants';

export const webpackCommonConfiguration: Configuration = {
  entry: PATHS.ENTRY,
  output: {
    filename: '[name].[contenthash].js',
    path: PATHS.OUTPUT,
    clean: true,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    symlinks: false,
    plugins: [new TsconfigPathsPlugin()],
  },
  optimization: {
    moduleIds: 'deterministic',
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          name: 'vendors',
          enforce: true,
        },
      },
    },
  },
  watchOptions: {
    ignored: /node_modules/,
  },
};
