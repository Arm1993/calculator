/**use in dev mod with ReactRefreshWebpackPlugin @see webpack.dev */
import ReactRefreshTypeScript from 'react-refresh-typescript';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import { IRule, TMode } from './interfaces';

export const getCodeRule = (mode: TMode): IRule => ({
  test: /\.tsx?$/,
  loader: 'ts-loader',
  exclude: [/node_modules/, /__tests__/, /.(spec|test).(tsx?$)/, /test-helpers/],
  options: {
    getCustomTransformers: () => ({
      before: [mode === 'development' && ReactRefreshTypeScript()].filter(Boolean),
    }),
    transpileOnly: mode === 'development',
  },
});

export const getStyleRule = (mode: TMode): IRule[] => {
  return [
    {
      test: /\.module.s?css$/,
      use: [
        mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            modules: {
              localIdentName: '[name]__[local]--[hash:base64:5]',
            },
          },
        },
        'postcss-loader',
        'sass-loader',
      ],
    },
    {
      test: /[^module]+\.s?css$/,
      use: [
        mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader',
        'postcss-loader',
        'sass-loader',
      ],
    },
  ];
};

export const getIconRule = (mode: TMode): IRule => ({
  test: /\.svg$/,
  use: [
    {
      loader: '@svgr/webpack',
      options: {
        prettier: false,
        svgo: false,
        svgoConfig: {
          plugins: [{ removeViewBox: false }],
        },
        titleProp: true,
        ref: true,
      },
    },
    'url-loader',
  ],
  generator: {
    filename: 'assets/icons/[hash][ext][query]',
  },
});

export const getImageRule = (mode: TMode): IRule => ({
  test: /\.(png|jpe?g|gif)$/,
  type: 'asset',
  generator: {
    filename: 'assets/images/[hash][ext][query]',
  },
});

export const getFontRule = (mode: TMode): IRule => ({
  test: /\.(woff|woff2)$/,
  type: 'asset',
  generator: {
    filename: 'assets/fonts/[hash][ext][query]',
  },
});
