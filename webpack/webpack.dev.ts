import { merge } from 'webpack-merge';
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin';

import { PATHS, PORT } from './constants';
import { IModifiedConfiguration } from './interfaces';
import {
  getCopyPluginInstance,
  getDefinePluginInstance,
  getESLintPluginInstance,
  getForkTsCheckerWebpackPluginInstance,
  getHtmlWebpackPluginInstance,
  getStylelintWebpackPlugin,
} from './plugins';
import { getImageRule, getCodeRule, getStyleRule, getFontRule, getIconRule } from './rules';
import { webpackCommonConfiguration } from './webpack.common';

const config: IModifiedConfiguration = {
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    static: PATHS.OUTPUT,
    port: PORT,
    hot: true,
    proxy: {
      '/api': {
        target: 'https://calculator-app-service.onrender.com',
        // target: 'http://localhost:3000',
        logLevel: 'debug',
        secure: false,
        changeOrigin: true,
      },
    },
  },
  stats: {
    loggingDebug: ['sass-loader'],
  },
  module: {
    rules: [
      getCodeRule('development'),
      ...getStyleRule('development'),
      getImageRule('development'),
      getFontRule('development'),
      getIconRule('development'),
    ],
  },
  plugins: [
    getHtmlWebpackPluginInstance('development'),
    getDefinePluginInstance('development'),
    /**use in dev mod with ReactRefreshTypeScript @see getCodeRule*/
    new ReactRefreshWebpackPlugin(),
    getForkTsCheckerWebpackPluginInstance('development'),
    getStylelintWebpackPlugin('development'),
    getESLintPluginInstance('development'),
    getCopyPluginInstance(),
  ],
  optimization: {
    minimize: false,
  },
};

export default merge(webpackCommonConfiguration, config);
