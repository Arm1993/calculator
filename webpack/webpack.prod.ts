import { merge } from 'webpack-merge';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';

import { IModifiedConfiguration } from './interfaces';
import {
  getCopyPluginInstance,
  getDefinePluginInstance,
  getESLintPluginInstance,
  getForkTsCheckerWebpackPluginInstance,
  getHtmlWebpackPluginInstance,
  getMiniCssExtractPluginInstance,
  getWorkboxPluginInstance,
} from './plugins';
import { getCodeRule, getStyleRule, getImageRule, getFontRule, getIconRule } from './rules';
import { webpackCommonConfiguration } from './webpack.common';

const config: IModifiedConfiguration = {
  mode: 'production',
  module: {
    rules: [
      getCodeRule('production'),
      ...getStyleRule('production'),
      getImageRule('production'),
      getFontRule('production'),
      getIconRule('production'),
    ],
  },
  plugins: [
    getHtmlWebpackPluginInstance('production'),
    getDefinePluginInstance('production'),
    getMiniCssExtractPluginInstance('production'),
    // getBundleAnalyzerPluginInstance("production"),
    getForkTsCheckerWebpackPluginInstance('production'),
    getWorkboxPluginInstance('production'),
    getCopyPluginInstance(),
    getESLintPluginInstance('development'),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      /**new syntax for extends default options */
      // eslint-disable-next-line quotes
      `...`,
      new CssMinimizerPlugin(),
    ],
  },
};

export default merge(webpackCommonConfiguration, config);
